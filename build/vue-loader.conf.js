'use strict'
const utils = require('./utils')
const config = require('../config')
const path = require('path')
const isProduction = process.env.NODE_ENV === 'production'
const sourceMapEnabled = isProduction
	? config.build.productionSourceMap
	: config.dev.cssSourceMap

module.exports = {
	/* loaders: Object.assign({},
		utils.cssLoaders({
			sourceMap: sourceMapEnabled,
			extract: isProduction
		}),{
			pug:[
				{
					loader: 'pug-plain-loader',
					options: {
						basedir: path.resolve(__dirname, '../src/pug/'),
						doctype:"html",
					}
				}
			]
		}
	), */
	loaders:{
		pug:[
			{
				loader: 'pug-plain-loader',
				options: {
					basedir: path.resolve(__dirname, '../src/pug/'),
					doctype:"html",
				}
			}
		],
		...utils.cssLoaders({
				sourceMap: sourceMapEnabled,
				extract: isProduction
			})
	},
	cssSourceMap: sourceMapEnabled,
	cacheBusting: config.dev.cacheBusting,
	transformToRequire: {
		video: ['src', 'poster'],
		source: 'src',
		img: 'src',
		image: 'xlink:href',
		vector: "src"
	}
}
