import cfg from "./config"

export function subir(contenido) {
	//console.log( "subir", cfg );

	const opciones = {
		method: "POST",
		//mode: 'no-cors',
		headers: cfg.encabezados,
		body: JSON.stringify(contenido)
	}
	//console.log(opciones);
	return fetch(cfg.accion("subir"), opciones)
		.then(cfg.controlRetorno)
		.then(datos => {
			//console.log("datos", datos);
			return datos
		})
}

export function guardar(valores) {
	console.log("guarda", valores)
	/*
	const opciones = {
		method: 'POST',
		//mode: 'no-cors',
		headers: cfg.encabezados,
		body: JSON.stringify( valores )
	};
	//console.log(opciones);
	return fetch( cfg.accion( "archivos" ), opciones )
		.then( cfg.controlRetorno )
		.then( datos => {
			console.log("datos", datos);
			return datos;
		});

	*/

	//console.log( "guardar", JSON.stringify( valores ));
	const opciones = {
		method: "post",
		//mode: 'no-cors',
		headers: cfg.encabezados,
		body: JSON.stringify(valores)
	}
	//console.log(opciones);
	return fetch(cfg.accion(`carpeta/${valores.carpeta}/archivos`), opciones)
		.then(cfg.controlRetorno)
		.then(datos => {
			//console.log("datos", datos);
			return datos
		})
}

export function actualizar(valores) {
	console.log("actualiza", valores)
	//console.log( "guardar", JSON.stringify( valores ));
	const opciones = {
		method: "POST",
		//mode: 'no-cors',
		headers: cfg.encabezados,
		body: JSON.stringify(valores)
	}
	//console.log(opciones);
	return fetch(cfg.accion(`archivo/${valores.id}`), opciones)
		.then(cfg.controlRetorno)
		.then(datos => {
			//console.log("datos", datos);
			return datos
		})
}

export function listar(id, taxonomia) {
	const opciones = {
		method: "GET",
		//mode: 'no-cors',
		headers: cfg.encabezados
		//body: JSON.stringify( valores )
	}
	//console.log(opciones);
	return fetch(cfg.accion(`${taxonomia}/${id}/archivos`), opciones)
		.then(cfg.controlRetorno)
		.then(datos => {
			console.log("datos", datos)
			return datos
		})
}

export function obtener(id) {
	const opciones = {
		method: "GET",
		//mode: 'no-cors',
		headers: cfg.encabezados
		//body: JSON.stringify( valores )
	}
	//console.log(opciones);
	return fetch(cfg.accion(`archivo/${id}`), opciones)
		.then(cfg.controlRetorno)
		.then(datos => {
			//console.log("datos", datos);
			return datos
		})
}

export function borrar(id) {
	const opciones = {
		method: "DELETE",
		//mode: 'no-cors',
		headers: cfg.encabezados
		//body: JSON.stringify( valores )
	}
	//console.log(opciones);
	return fetch(cfg.accion(`archivo/${id}`), opciones)
		.then(cfg.controlRetorno)
		.then(datos => {
			//console.log("datos", datos);
			return datos
		})
}

export function descargar(id) {
	//console.log(cfg.accion( `archivo/${id}/descargar` ))
	//console.log("W", W);
	window.open(cfg.accion(`archivo/${id}/descargar`))
}

export function buscar(categoria, valor) {
	const opciones = {
		method: "GET",
		//mode: 'no-cors',
		headers: cfg.encabezados
		//body: JSON.stringify( valores )
	}
	//console.log(opciones);
	const buscar = "?v=" + encodeURIComponent(valor)

	return fetch(cfg.accion(`buscar/${categoria}${buscar}`), opciones)
		.then(cfg.controlRetorno)
		.then(datos => {
			//console.log("datos", datos);
			return datos
		})
}

export function favoritos(usuario) {
	const opciones = {
		method: "GET",
		//mode: 'no-cors',
		headers: cfg.encabezados
		//body: JSON.stringify( valores )
	}
	//console.log(opciones);

	return fetch(cfg.accion(`usuario/${usuario}/favoritos`), opciones)
		.then(cfg.controlRetorno)
		.then(datos => {
			//console.log("datos", datos);
			return datos
		})
}

export function marcar(usuario, archivo, marcar) {
	const opciones = {
		method: "POST",
		//mode: 'no-cors',
		headers: cfg.encabezados,
		body: JSON.stringify({ id: archivo, marcar })
	}
	//console.log(opciones);

	return fetch(cfg.accion(`usuario/${usuario}/favoritos`), opciones)
		.then(cfg.controlRetorno)
		.then(datos => {
			//console.log("datos", datos);
			return datos
		})
}

export function previo(archivo) {
	return cfg.accion(`archivo/${archivo}/previo`)
}

export function enlace(id) {
	//console.log( "obteniendo carpeta", id );
	const opciones = {
		method: "GET",
		//mode: 'no-cors',
		headers: cfg.encabezados
	}
	//console.log(opciones);
	return fetch(cfg.accion(`archivo/${id}/enlace`), opciones)
		.then(cfg.controlRetorno)
		.then(datos => {
			return datos
		})
}
