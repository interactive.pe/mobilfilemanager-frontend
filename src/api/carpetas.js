import cfg from "./config"

export function listar(id) {
	const opciones = {
		method: "GET",
		//mode: 'no-cors',
		headers: cfg.encabezados
	}
	//console.log(opciones);
	return fetch(cfg.accion(`usuario/${id}/carpetas`), opciones)
		.then(cfg.controlRetorno)
		.then(datos => {
			return datos
		})
}

export function obtener(id) {
	//console.log( "obteniendo carpeta", id );
	const opciones = {
		method: "GET",
		//mode: 'no-cors',
		headers: cfg.encabezados
	}
	//console.log(opciones);
	return fetch(cfg.accion(`carpeta/${id}`), opciones)
		.then(cfg.controlRetorno)
		.then(datos => {
			return datos
		})
}

export function crear(id, nombre, contenedora) {
	const opciones = {
		method: "POST",
		headers: cfg.encabezados,
		body: JSON.stringify({ nombre, contenedora })
	}
	console.log(cfg.accion(`usuario/${id}/carpetas`))
	console.log(opciones)
	return fetch(cfg.accion(`usuario/${id}/carpetas`), opciones)
		.then(cfg.controlRetorno)
		.then(datos => {
			return datos
		})
}

export function borrar(id) {
	const opciones = {
		method: "DELETE",
		headers: cfg.encabezados
	}
	return fetch(cfg.accion(`carpeta/${id}`), opciones)
		.then(cfg.controlRetorno)
		.then(datos => {
			return datos
		})
}

export function enlace(id) {
	//console.log( "obteniendo carpeta", id );
	const opciones = {
		method: "GET",
		//mode: 'no-cors',
		headers: cfg.encabezados
	}
	//console.log(opciones);
	return fetch(cfg.accion(`carpeta/${id}/enlace`), opciones)
		.then(cfg.controlRetorno)
		.then(datos => {
			return datos
		})
}
