const wl = window.location

let protocolo = wl.protocol
let servidor = wl.hostname.replace("cliente.", "www.")
let directorio = wl.pathname == "/" ? "" : wl.pathname

//const urlbase = "//127.0.0.1/api"
//const urlbase = "//www.proyecto.desarrollo/api"
//const urlbase = "//140.82.28.69/repositorio/api"
//const urlbase = "//mobilfilemanager.com/api"
const urlbase = `//${servidor}${directorio}/api`

function accion(nombre) {
	return `${urlbase}/${nombre}`
}

function controlRetorno(response) {
	//console.log("control", response)
	let error
	switch (response.status) {
		case 409:
			//error = (data && data.message) || response.statusText;
			//console.log( error );
			return Promise.reject(response)
			break
		case 401:
			//const error = (data && data.message) || response.statusText;
			return Promise.reject(response.statusText)
			break
		case 200:
			return response.text().then(texto => {
				try {
					const datos = texto && JSON.parse(texto)
					return datos
				} catch (error) {
					console.error("ERROR", error, texto)
					return Promise.reject(error)
				}
			})

			break
		default:
			error = (data && data.message) || response.statusText
			return Promise.reject(error)
			break
	}
}

export default {
	accion,
	encabezados: {
		"Content-Type": "application/json",
		Authorization: "Bearer RXVnZW5pYSB0ZSBleHRyYcOxbw=="
	},
	controlRetorno
}
