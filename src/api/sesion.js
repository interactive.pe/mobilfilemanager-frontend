import cfg from "./config"

export function iniciar( usuario, clave ) {
	const opciones = {
		method: 'POST',
		//mode: 'no-cors',
		headers: cfg.encabezados,
		body: JSON.stringify({ usuario, clave })
	};
	//console.log(opciones);
	return fetch( cfg.accion( "autenticar" ), opciones )
		.then( controlRetorno )
		.then( datos => {
			console.log( "datos", datos );
			return datos.sesion;
		});

}

function controlRetorno(response) {
	//console.log("control", response);
	switch ( response.status ){
	case 500:
		const error = ( data && data.message ) || response.statusText;
		return Promise.reject( error );
		break;
	case 401:
		//const error = (data && data.message) || response.statusText;
		return Promise.reject( response.statusText );
		break;

	default:
		return response.text().then( texto =>{
			const datos = texto && JSON.parse( texto );
			return( datos )
		});
		break;
	}

}
