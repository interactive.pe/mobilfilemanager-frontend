import cfg from "./config"

export function perfil( id ) {
	const opciones = {
		method: 'GET',
		headers: cfg.encabezados,
	};
	return fetch( cfg.accion( `usuario/${id}/perfil` ), opciones )
		.then( cfg.controlRetorno )
		.then( datos => {
			return datos;
		});
}

export function guardar( id, datos ) {
	const opciones = {
		method: 'POST',
		headers: cfg.encabezados,
		body: JSON.stringify( datos )
	};
	return fetch( cfg.accion( `usuario/${id}/perfil` ), opciones )
		.then( cfg.controlRetorno )
		.then( datos => {
			return datos;
		});
}
