import cfg from "./config"

export function listar() {
	//console.log( "etiquetas.obtener", cfg.accion( "etiquetas" ));
	const opciones = {
		method: "GET",
		headers: cfg.encabezados
	}

	return fetch(cfg.accion("etiquetas"), opciones)
		.then(cfg.controlRetorno)
		.then(datos => {
			return datos
		})
}

export function obtener(id) {
	const opciones = {
		method: "GET",
		headers: cfg.encabezados
	}
	return fetch(cfg.accion(`etiqueta/${id}`), opciones)
		.then(cfg.controlRetorno)
		.then(datos => {
			return datos
		})
}

export function todas() {
	console.log("etiquetas.todas", cfg.accion("etiquetas"))
	const opciones = {
		method: "GET",
		headers: cfg.encabezados
	}

	return fetch(cfg.accion("etiquetas/todas"), opciones)
		.then(cfg.controlRetorno)
		.then(datos => {
			return datos
		})
}
