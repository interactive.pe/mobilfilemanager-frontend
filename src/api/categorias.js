import cfg from "./config"

export function listar() {
	const opciones = {
		method: 'GET',
		//mode: 'no-cors',
		headers: cfg.encabezados
	};
	//console.log(opciones);
	return fetch(cfg.accion("categorias"), opciones)
		.then(cfg.controlRetorno)
		.then(datos => {
			//console.log("datos", datos);
			return datos;
		});

}


export function obtener( id ) {
	const opciones = {
		method: 'GET',
		//mode: 'no-cors',
		headers: cfg.encabezados
	};
	//console.log(opciones);
	return fetch( cfg.accion( `categoria/${id}` ), opciones )
		.then( cfg.controlRetorno )
		.then(datos => {
			return datos;
		});
}
