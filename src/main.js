// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue"

import App from "./App"
import store from "./store"
import router from "./router"

import "./filtros.js"

import VueWorker from "vue-worker"
Vue.use(VueWorker, "$worker")

import feather from "vue-icon"
Vue.use(feather, "v-icon")

import vector from "@/components/Vector"
Vue.component("vector", vector)

import VoerroTagsInput from "@voerro/vue-tagsinput"
Vue.component("tags-input", VoerroTagsInput)

import vmodal from "vue-js-modal"
Vue.use(vmodal)

import VueCroppie from "vue-croppie"
Vue.use(VueCroppie)

import VTooltip from "v-tooltip"
Vue.use(VTooltip)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
	el: "#app",
	store,
	router,
	components: { App },
	template: "<App/>"
})
