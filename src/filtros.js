import Vue from 'vue'

// Inicial Mayuscula
Vue.filter('capitalizar', function (value) {
	if (!value) return ''
	value = value.toString()
	return value.charAt(0).toUpperCase() + value.slice(1)
})
