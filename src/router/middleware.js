export function log({ next, to, router }) {
	console.log( "log", to.name );
}

export function auth({ router, store, next }) {
	if( !store.state.usuario.id ){
		return router.push({ name: 'login' });
	}
	return next();
}
