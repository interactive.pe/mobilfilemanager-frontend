import Vue from "vue"
import Router from "vue-router"
import Login from "@/components/Login"
import Escritorio from "@/components/Escritorio"
import Disco from "@/components/Disco"
import Carpeta from "@/components/Carpeta"
import Categoria from "@/components/Categoria"
import Etiqueta from "@/components/Etiqueta"
import Perfil from "@/components/Perfil"
import Busqueda from "@/components/Busqueda"
import Favoritos from "@/components/Favoritos"
import Etiquetas from "@/components/Etiquetas"

import store from "@/store"
import { auth, log } from "./middleware"

Vue.use(Router)

const router = new Router({
	//base: __dirname,
	routes: [
		{
			path: "/",
			name: "inicio",
			redirect: "escritorio"
		},
		{
			path: "/login",
			name: "login",
			component: Login
		},
		{
			path: "/escritorio",
			name: "escritorio",
			component: Escritorio,
			beforeEnter(to, from, next) {
				auth({ router, store, next })
			}
		},
		{
			path: "/disco",
			component: Disco,
			beforeEnter(to, from, next) {
				auth({ router, store, next })
			},
			children: [
				{
					path: "",
					name: "disco",
					component: Carpeta
				},
				{
					path: "categoria/:id",
					name: "categoria",
					component: Categoria
				},
				{
					path: "carpeta/:id",
					name: "carpeta",
					component: Carpeta
				},
				{
					path: "etiqueta/:id",
					name: "etiqueta",
					component: Etiqueta
				},
				{
					path: "/favoritos",
					name: "favoritos",
					component: Favoritos
				},
				{
					path: "/etiquetas",
					name: "etiquetas",
					component: Etiquetas
				}
			]
		},
		{
			path: "/perfil",
			name: "perfil",
			component: Perfil
		},
		{
			path: "/busqueda",
			name: "busqueda",
			component: Busqueda
		}
	],
	linkActiveClass: "activo",
	linkExactActiveClass: "actual"
})

router.beforeEach((to, from, next) => {
	//console.log( to.matched );
	//next();

	/*
	if ( to.meta.middleware ) {
		const middleware = Array.isArray(to.meta.middleware)
			? to.meta.middleware
			: [to.meta.middleware];

		const contexto = {
			from,
			next,
			router,
			store,
			to
		};

		const siguiente = nextFactory( contexto, middleware );
		return middleware[0]({ ...contexto, next: siguiente });
	}
	*/
	return next()
})

/*
function nextFactory( contexto, middleware, indice = 1 ) {
	const consecutivo = middleware[ indice ];
	if ( !consecutivo ) return contexto.next

	return ( ...parameters ) => {
		contexto.next( ...parameters );
		const siguiente = nextFactory( contexto, middleware, indice + 1 );
		consecutivo({ ...contexto, next: siguiente });
	}
}
*/

export default router
