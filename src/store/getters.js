import Vue from "vue"
import { getters } from "vuex"
import { archivos } from "@/api"

export default {
	esFavorito: state => id => state.favoritos.some(archivo => archivo.id == id),
	previo: (state, getters) => id => {
		const archivo = state.archivos.find(archivo => archivo.id == id)

		if (archivo) {
			return archivos.previo(archivo.id)
		} else {
			return `#${archivo.nombre}.${archivo.tipo}`
		}
	},
	esImagen: state => tipo => {
		const tipos = ["png", "jpg", "jpeg", "webp", "gif"]
		return tipos.includes(tipo)
	},
	alias: state => state.usuario.nombre || `${state.perfil.nombre} ${state.perfil.apellidos}`,
	nombreCompleto: state => `${state.perfil.nombre} ${state.perfil.apellidos}`
}
