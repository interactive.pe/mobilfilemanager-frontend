import Vue from 'vue'
import { archivos } from '@/api';


export default {
	actualizaSesion( state, sesion ){
		//console.log("actualizaSesion", sesion);
		state.sesion.token = sesion.token;
		state.usuario = sesion.usuario;
		state.disco = sesion.disco;
		state.carpeta = sesion.disco;
	},
	cerrarSesion( state ){
		state.sesion.token = "";
		state.usuario = {};
		state.disco = null;
		state.carpeta = null;
	},
	actualizaCategorias( state, categorias ){
		state.categorias = categorias.descendientes;
	},
	actualizarCarpetas( state, carpetas ){
		state.carpetas = carpetas;
	},
	actualizarCarpeta( state, carpeta ){
		state.carpeta = carpeta;
	},
	actualizaArchivos( state, archivos ){
		state.archivos = archivos;
	},
	actualizaArchivo( state, archivo ){
		state.archivo = archivo;
		console.debug("actualizaArchivo", archivo);
	},
	actualizarCategoria( state, categoria ){
		state.categoria = categoria;
	},


	seleccionarCarpeta( state, id ){
		//console.log( "MUTATION seleccionarCarpeta" );
		//const seleccionada = state.carpetas.filter( carpeta => carpeta.id == id )[0];
		//state.carpeta = seleccionada || state.disco;
	},
	seleccionarArchivo( state, id ){
		//console.log( "MUTATION seleccionarArchivo" );
		//state.archivo = state.archivos.filter( archivo => archivo.id == id )[0];
	},
	actualizarEtiquetas( state, etiquetas ){
		state.etiquetas = etiquetas;
	},
	seleccionarTaxonomia( state, taxonomia ){
		//console.log( "tax", taxonomia );
		state.taxonomia = taxonomia;
	},
	actualizarEtiqueta( state, etiqueta ){
		state.etiqueta = etiqueta;
	},
	actualizarResultados( state, resultados ){
		state.resultados = resultados;
	},
	actualizarFavoritos( state, favoritos ){
		state.favoritos = favoritos;
	},
	actualizarPerfil( state, perfil ){
		state.perfil = perfil;
	},
}
