import Vue from "vue"
import { sesion, usuarios, categorias, carpetas, archivos, etiquetas } from "@/api"
import router from "@/router"

export function autenticar({ dispatch, commit }, { usuario, clave, alerta }) {
	//commit('loginRequest', { username });

	sesion.iniciar(usuario, clave).then(
		sesion => {
			commit("actualizaSesion", sesion)
			dispatch("obtenerCarpetas")
			dispatch("obtenerFavoritos")
			dispatch("obtenerPerfil")
			router.push({ name: "escritorio" })
		},
		error => {
			//commit('loginFailure', error);
			//dispatch('alert/error', error, { root: true });
			if (alerta) alerta.call(null, error)
		}
	)

	//sesion.iniciar(usuario, clave);
	//console.log(sesion.iniciar);
}

export function obtenerCategorias({ dispatch, commit }) {
	categorias.listar().then(
		categorias => {
			commit("actualizaCategorias", categorias)
		},
		error => {}
	)
}

export function obtenerArchivos({ commit, state }, { id }) {
	archivos.listar(id, state.taxonomia).then(
		archivos => {
			commit("actualizaArchivos", archivos)
		},
		error => {}
	)

	//sesion.iniciar(usuario, clave);
	//console.log(sesion.iniciar);
}

export function obtenerCarpetas({ commit, state }) {
	//console.log( "obtener carpetas de", state.usuario.nombre );
	carpetas.listar(state.usuario.id).then(
		carpetas => {
			commit("actualizarCarpetas", carpetas)
		},
		error => {}
	)
}

export function obtenerCarpeta({ dispatch, commit, state }, { id, actualiza }) {
	if (!actualiza) {
		commit("actualizarCarpeta", false)
		commit("actualizaArchivos", [])
		commit("actualizaArchivo", false)
	}
	carpetas.obtener(id).then(
		carpeta => {
			commit("actualizarCarpeta", carpeta)
			//obtenerArchivos({ commit, state }, { id });
			dispatch("obtenerArchivos", { id })
		},
		error => {}
	)
}

export function obtenerCategoria({ dispatch, commit, state }, { id, actualiza }) {
	if (!actualiza) {
		commit("actualizarCategoria", false)
		commit("actualizaArchivos", [])
		commit("actualizaArchivo", false)
	}
	categorias.obtener(id).then(
		categoria => {
			commit("actualizarCategoria", categoria)
			//obtenerArchivos({ commit, state }, { id });
			dispatch("obtenerArchivos", { id })
		},
		error => {}
	)
}

export function obtenerEtiqueta({ dispatch, commit, state }, { id, actualiza }) {
	if (!actualiza) {
		commit("actualizarEtiqueta", false)
		commit("actualizaArchivos", [])
		commit("actualizaArchivo", false)
	}
	etiquetas.obtener(id).then(
		etiqueta => {
			commit("actualizarEtiqueta", etiqueta)
			dispatch("obtenerArchivos", { id })
		},
		error => {}
	)
}

export function obtenerArchivo({ commit, state }, { id }) {
	commit("actualizaArchivo", false)
	archivos.obtener(id).then(
		archivo => {
			commit("actualizaArchivo", archivo)
		},
		error => {}
	)
}

export function borrarCarpeta({ dispatch, commit, state }) {
	//alert( "borrar " + state.carpeta.id );
	//return;
	carpetas.borrar(state.carpeta.id).then(
		resultado => {
			//commit('actualizarCarpeta', carpeta);
			//obtenerCarpetas({ commit, state });
			dispatch("obtenerCarpetas")
			router.push({ name: "carpeta", params: { id: resultado.contenedora } })
		},
		error => {}
	)
}

export function borrarArchivo({ dispatch, commit, state }, { id }) {
	//alert( "borrar " + state.carpeta.id );
	//return;
	archivos.borrar(id).then(
		resultado => {
			//commit('actualizarCarpeta', carpeta);
			//const idCarpeta = state.carpeta.id;
			//obtenerArchivos({ commit, state }, { id: idCarpeta });
			dispatch("obtenerArchivos", { id: state.carpeta.id })
			//obtenerCarpetas({ commit, state });
		},
		error => {}
	)
}

export function crearCarpeta({ dispatch, commit, state }, nombre) {
	const id = state.usuario.id
	const contenedora = state.carpeta.id
	//alert( "borrar " + state.carpeta.id );
	//return;
	carpetas.crear(id, nombre, contenedora).then(
		resultado => {
			//commit('actualizarCarpeta', carpeta);
			console.log(resultado)
			//obtenerCarpetas({ commit, state });
			dispatch("obtenerCarpetas")
			//router.push({ name:"carpeta", params: { id: resultado.contenedora }});
		},
		error => {
			console.log(error)
		}
	)
}

export function obtenerEtiquetas({ commit, state }) {
	etiquetas.listar().then(
		etiquetas => {
			//console.log( "EEEE", etiquetas );
			commit("actualizarEtiquetas", etiquetas)
		},
		error => {
			console.log(error)
		}
	)
}

export function buscarArchivos({ commit, state }, { categoria, valor }) {
	archivos.buscar(categoria, valor).then(
		resultados => {
			//console.log( "EEEE", etiquetas );
			commit("actualizarResultados", resultados)
		},
		error => {
			console.log(error)
		}
	)
}

export function obtenerFavoritos({ commit, state }) {
	archivos.favoritos(state.usuario.id).then(
		favoritos => {
			//console.log( "EEEE", etiquetas );
			commit("actualizarFavoritos", favoritos)
		},
		error => {
			console.log(error)
		}
	)
}

export function marcarArchivo({ dispatch, commit, state }, { id, marcar }) {
	archivos.marcar(state.usuario.id, id, marcar).then(
		resultado => {
			//commit('actualizarFavoritos', favoritos);
			commit("actualizaArchivo", false)
			dispatch("obtenerFavoritos")
		},
		error => {
			console.log(error)
		}
	)
}

export function obtenerPerfil({ commit, state }) {
	usuarios.perfil(state.usuario.id).then(
		perfil => {
			//commit('actualizarFavoritos', favoritos);
			commit("actualizarPerfil", perfil)
		},
		error => {
			console.log(error)
		}
	)
}

export function guardarPerfil({ dispatch, state }, datos) {
	usuarios.guardar(state.usuario.id, datos).then(
		resultado => {
			dispatch("obtenerPerfil")
		},
		error => {
			console.log(error)
		}
	)
}
