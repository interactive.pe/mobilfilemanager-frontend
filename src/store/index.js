import Vue from "vue"
import Vuex from "vuex"
import state from "./state"
import getters from "./getters"
import * as actions from "./actions"
import mutations from "./mutations"

import VuexPersist from "vuex-persist"

Vue.use(Vuex)

const vuexLocalStorage = new VuexPersist({
	key: "docboxVuex", // The key to store the state on in the storage provider.
	storage: window.localStorage // or window.sessionStorage or localForage
	// Function that passes the state and returns the state with only the objects you want to store.
	// reducer: state => state,
	// Function that passes a mutation and lets you decide if it should update the state in localStorage.
	// filter: mutation => (true)
})

const debug = process.env.NODE_ENV !== "production"

export default new Vuex.Store({
	state,
	getters,
	actions,
	mutations,
	strict: false,
	plugins: [vuexLocalStorage.plugin]
})
