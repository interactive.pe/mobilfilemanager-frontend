export default {
	sesion: {
		token: null,
	},
	usuario: {
		id: null,
		token: null,
		nombre: "",
		correo: "",
	},
	disco: {},
	categorias: [],
	categoria: null,
	carpetas: [],
	carpeta: null,
	archivos: [],
	archivo: null,
	etiquetas: {},
	favoritos: [],
	taxonomia:"carpeta",
	resultados:[],
	perfil: {}
}
