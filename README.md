# docbox-cliente

> Cliente de DocBox


## Tecnologías
- [Node](https://nodejs.org/es/)
- [Webpack](https://webpack.js.org/)
- [Vue](https://vuejs.org/)
- [Pug](https://pugjs.org/api/getting-started.html)
- [ES6](http://es6-features.org/)
- [Stylus](http://stylus-lang.com/)

## Instrucciones

``` bash
# install dependencies
npm install

# Ejecutar Servidor de Desarrollo localhost:8080
npm run dev

# Compilación para producción
npm run build

# Compilación para producción y generar reporte de análisis
npm run build --report
```

> Basado en la plantilla [Vue Webpack](http://vuejs-templates.github.io/webpack/) con [vue-loader](http://vuejs.github.io/vue-loader).

## Archivos y directorios
- **index.html**: html general
- **src/**: contiene el codigo de la aplicación
	- **src/main.js**: Archivo de arranque
	- **src/App.vue**: Componente principal
	- **src/assets/**: Imágenes y otros archivos
	- **src/components/**: Componentes de la aplicacion
	- **src/router/index.js**: Rutas de la aplicación
	- **src/pug/**: Archivos auxiliares de [Pug](https://pugjs.org/api/getting-started.html)
	- **src/stylus/**: Archivos auxiliares de [Stylus](http://stylus-lang.com/)
	- **src/stylus/general.styl**: Esta hoja de estilos afecta a toda la aplicación
- **static/**: Aquí van los archivos que no se procesan con webpack
- **public/**: Al compilar se genera esta carpeta, esta es la que se publica en el servidor

## Estructura de los componentes
> Cada archivo .vue puede tener 3 partes: Template, Script, Style

### \<template>
Contiene la plantilla de contenido que va a mostrar el componente

**En pug**:
```pug
<template lang="pug">
	.itemFichero
		.icono: .ico(:data-ext="archivo.extension")
		p {{ archivo.nombre }}
</template>
```


**En html**:
```html
<template>
	<div id="app">
		<encabezado/>
		<main>
			<router-view/>
		</main>
		<pie/>
	</div>
</template>
```
> Mas información en https://vuejs.org/v2/guide/syntax.html

### \<script>
Contiene el código del componente<br>
Se recomienda usar [ES6](http://es6-features.org/)
```javascript
<script>
import Buscar from "@/components/Buscar"
import ItemFichero from "@/components/ItemFichero"
import ItemComentario from "@/components/ItemComentario"

export default {
	name:"Escritorio",
	components:{
		Buscar,
		"item": ItemFichero,
		"comentario": ItemComentario
	},
	data(){
		return { info }
	}
}
</script>
```
> Mas información en https://vuejs.org/v2/guide/components.html

### \<style>
Contiene los stylos del componente<br>
Se recomienda usar el atributo ```scoped``` para evitar conflictos con otras clases<br>

**En Stylus**
```stylus
<style lang="stylus" scoped>
@import '~nib/index'
@import "../stylus/base/utiles.styl"

#escritorio
	display flex
	flex-direction column
	justify-content space-around
	//-padding-bottom 1rem

.busqueda
	flex 1 1 50%

#ficheros
	flex 0 0 50%
	margin 2rem
	padding 1rem
	border-radius .5rem
	background-color blanco
	display grid
	grid-gap 1rem
	grid-template-areas "favoritos favoritos" \
		"recientes comentarios"
	grid-template-columns 3fr 1fr
	box-shadow 0 2px 8px 0 alpha( negro, 25% )

h2
	text-align left
	margin-bottom .5rem

</style>
```

**En Css**
```css
<style>
@import "https://cdnjs.cloudflare.com/ajax/libs/10up-sanitize.css/7.0.3/sanitize.min.css";

#app{
	font-family: 'Avenir', Helvetica, Arial, sans-serif;
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;
	color: blue;
	height: 100vh;
	display grid
	grid-template-areas: "encabezado"
		"contenido"
		"pie";
	grid-template-rows: 80px 1fr 60px;
	justify-content: stretch;
	align-items: stretch;
}

#encabezado{
	grid-area: encabezado;
}

main{
	grid-area: contenido;
	overflow: auto;
	display: flex;
	align-items: stretch;
	background-color: blue;
}

#pie{
	grid-area: pie;
}

</style>
```
> Mas información en https://vuejs.org/v2/guide/class-and-style.html
